CREATE TABLE "[prefix]bans" (
    "id" BIGSERIAL NOT NULL,
    "type" CHAR(4) NOT NULL,
    "value" VARCHAR(15) NOT NULL,
    "punisher" VARCHAR(15) NOT NULL,
    "reason" TEXT NOT NULL,
    "created_at" TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    "expire_at" TIMESTAMPTZ NULL,

    PRIMARY KEY ("id"),
    UNIQUE ("type", "value")
);

CREATE INDEX ON "[prefix]bans" USING hash ("punisher");
CREATE INDEX ON "[prefix]bans" ("expire_at" ASC NULLS LAST);

CREATE TABLE "[prefix]strikes" (
    "id" BIGSERIAL NOT NULL,
    "punisher" VARCHAR(15) NOT NULL,
    "user" VARCHAR(15) NOT NULL,
    "created_at" TIMESTAMPTZ NOT NULL DEFAULT NOW(),

    PRIMARY KEY ("id")
);