CREATE TABLE [prefix]bans (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    type VARCHAR NOT NULL,
    value VARCHAR NOT NULL,
    punisher VARCHAR NOT NULL,
    reason TEXT NOT NULL,
    created_at DATETIME NOT NULL DEFAULT (datetime()),
    expire_at DATETIME NULL,

    UNIQUE (type, value)
);

CREATE TABLE [prefix]strikes (
   id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
   punisher VARCHAR NOT NULL,
   user VARCHAR NOT NULL,
   created_at DATETIME NOT NULL DEFAULT (datetime())
);