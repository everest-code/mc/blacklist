package co.everestcode.BlackList.Timer;

import co.everestcode.BlackList.Storage.Storage;

public class DeleteBansTimer implements Runnable {
    private Storage store;

    public DeleteBansTimer(Storage store) {
        this.store = store;
    }

    @Override
    public void run() {
        try {
            store.removeExpiredBans();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
