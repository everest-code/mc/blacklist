package co.everestcode.BlackList.Command;

import co.everestcode.BlackList.Storage.Storage;
import co.everestcode.BlackList.Utils.LuckPermsAccessor;
import co.everestcode.BlackList.Utils.Utils;
import net.kyori.adventure.text.Component;
import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;
import org.bukkit.Server;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.util.Calendar;
import java.util.Date;
import java.util.Objects;
import java.util.logging.Logger;

public class StrikeCommand implements CommandExecutor {
    private Logger log;
    private Storage store;
    private Server server;
    private int maxStrikes;
    private int timeout;
    private int banTimeout;

    public StrikeCommand(Logger log, Storage store, Server server, int maxStrikes, int timeout, int banTimeout) {
        this.log = log;
        this.store = store;
        this.server = server;
        this.maxStrikes = maxStrikes;
        this.timeout = timeout;
        this.banTimeout = banTimeout;
    }

    protected ArgumentParser createArgParser() {
        ArgumentParser args = ArgumentParsers.newFor("blacklist.strike")
                .build()
                .defaultHelp(false);

        args.addArgument("user")
                .help("User name.");

        return args;
    }
    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {
        String senderName = "<internal>";
        if (sender instanceof Player p) {
            if (!LuckPermsAccessor.hasAccessTo(p.getUniqueId(), "blacklist.strike")) {
                sender.sendMessage("\u00A7c\u00A7lERROR:\u00A7r You doesn't have access to this command");
                return false;
            }
            senderName = p.getName();
        }

        Namespace ns;
        try {
            ns = createArgParser().parseArgs(Utils.arrayParse(args));
        } catch (ArgumentParserException e) {
            String message = e.getMessage();
            if (message.equals("Help Screen")) {
                message = createArgParser().formatHelp();
            }
            sender.sendMessage(String.format("\u00A7c\u00A7lERROR:\u00A7r %s", message));
            return false;
        }

        String user = ns.getString("user");

        try {
            store.createStrike(senderName, user);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        try {
            int count = store.countStrikes(user, timeout);
            Player player = Objects.requireNonNull(server.getPlayer(user));
            player.sendMessage(String.format("\u00A76\u00A7lWARN:\u00A7r \u00A7nHey %s, be careful you have %d/%d strikes.", user, count, maxStrikes));

            if (count >= maxStrikes) {
                sender.sendMessage(String.format("\u00A73\u00A7lINFO:\u00A7r Ban for %s was created.", user));
                String reason = "Banned for exceeding the number of strikes";
                Calendar cal = Calendar.getInstance();
                cal.setTime(new Date());
                cal.add(Calendar.MINUTE, banTimeout);
                store.createBan("user", user, senderName, reason, cal.getTime());
                if (player.isOnline()) {
                    player.kick(Component.text(reason));
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return true;
    }
}
