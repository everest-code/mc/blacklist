package co.everestcode.BlackList.Command;

import co.everestcode.BlackList.Storage.Storage;
import co.everestcode.BlackList.Utils.LuckPermsAccessor;
import co.everestcode.BlackList.Utils.Utils;
import net.kyori.adventure.text.Component;
import net.luckperms.api.LuckPerms;
import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;
import org.bukkit.Server;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;
import java.util.logging.Logger;

public class BlacklistCommand implements CommandExecutor {
    private Logger log;
    private Storage store;
    private Server server;

    public BlacklistCommand(Logger log, Storage store, Server server) {
        this.log = log;
        this.store = store;
        this.server = server;
    }

    protected ArgumentParser createArgParser() {
        ArgumentParser args = ArgumentParsers.newFor("blacklist.black")
                .build()
                .defaultHelp(false);
        args.addArgument("type")
                .choices("ip", "user")
                .help("Type of ban");

        args.addArgument("value")
                .help("Value of ban");

        args.addArgument("-r", "--reason")
                .dest("reason")
                .nargs("?")
                .setDefault(store.getMessage("blacklist"))
                .help("Reason because the player was banned");

        return args;
    }

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {
        String senderName = "<internal>";
        Integer weight = null;
        if (sender instanceof Player p) {
            if (!LuckPermsAccessor.hasAccessTo(p.getUniqueId(), "blacklist.blacklist")) {
                sender.sendMessage("\u00A7c\u00A7lERROR:\u00A7r You doesn't have access to this command");
                return false;
            }
            weight = LuckPermsAccessor.getUserWeight(p.getUniqueId());
            senderName = p.getName();
        }

        Namespace ns;
        try {
            ns = createArgParser().parseArgs(Utils.arrayParse(args));
        } catch (ArgumentParserException e) {
            String message = e.getMessage();
            if (message.equals("Help Screen")) {
                message = createArgParser().formatHelp();
            }
            sender.sendMessage(String.format("\u00A7c\u00A7lERROR:\u00A7r %s", message));
            return false;
        }

        if (weight != null && ns.getString("type") == "user") {
            int bweight = LuckPermsAccessor.getUserWeight(Objects.requireNonNull(server.getPlayer(ns.getString("value"))).getUniqueId());
            if (weight <= bweight) {
                sender.sendMessage("\u00A7c\u00A7lERROR:\u00A7r You can't ban this user");
                return false;
            }
        }

        int id;
        try {
            id = store.createBan(
                    ns.getString("type"),
                    ns.getString("value"),
                    senderName,
                    ns.getString("reason")
            );
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        sender.sendMessage(String.format("\u00A7c\u00A72OK:\u00A7r \u00A7nBlacklist entry #%d was created.", id));

        Utils.kickUsers(ns, server);

        return true;
    }
}
