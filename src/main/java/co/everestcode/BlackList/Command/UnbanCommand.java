package co.everestcode.BlackList.Command;

import co.everestcode.BlackList.Storage.Storage;
import co.everestcode.BlackList.Utils.LuckPermsAccessor;
import co.everestcode.BlackList.Utils.Utils;
import net.luckperms.api.LuckPerms;
import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.util.logging.Logger;

public class UnbanCommand implements CommandExecutor {
    private Logger log;
    private Storage store;
    private String name;

    public UnbanCommand(Logger log, Storage store) {
        this.log = log;
        this.store = store;
        this.name = "Ban";
    }

    public UnbanCommand(Logger log, Storage store, String name) {
        this.log = log;
        this.store = store;
        this.name = name;
    }

    public String getPermission() {
        return (name == "Ban")? "blacklist.unban" : "blacklist.unblack";
    }

    protected ArgumentParser createArgParser() {
        ArgumentParser args = ArgumentParsers.newFor(getPermission())
                .build()
                .defaultHelp(false);

        args.addArgument("value")
                .help("Value or id of ban to delete");

        return args;
    }

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {
        if (sender instanceof Player p) {
            if (!LuckPermsAccessor.hasAccessTo(p.getUniqueId(), getPermission())) {
                sender.sendMessage("\u00A7c\u00A7lERROR:\u00A7r You doesn't have access to this command");
                return false;
            }
        }

        Namespace ns;
        try {
            ns = createArgParser().parseArgs(Utils.arrayParse(args));
        } catch (ArgumentParserException e) {
            String message = e.getMessage();
            if (message.equals("Help Screen")) {
                message = createArgParser().formatHelp();
            }
            sender.sendMessage(String.format("\u00A7c\u00A7lERROR:\u00A7r %s", message));
            return false;
        }

        String value = ns.getString("value");

        try {
            int id = Integer.parseInt(value);
            store.deleteBan(id);
            sender.sendMessage(String.format("\u00A7c\u00A72OK:\u00A7r \u00A7n%s #%d was daleted.", name, id));
        } catch (NumberFormatException ex) {
            try {
                store.deleteBan(value);
                sender.sendMessage(String.format("\u00A7c\u00A72OK:\u00A7r \u00A7n%s value '%s' was deleted.", name, value));
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return true;
    }
}
