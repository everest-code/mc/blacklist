package co.everestcode.BlackList.Command;

import co.everestcode.BlackList.Storage.Storage;
import co.everestcode.BlackList.Utils.LuckPermsAccessor;
import co.everestcode.BlackList.Utils.Utils;
import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;
import org.bukkit.Server;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;
import java.util.logging.Logger;

public class ViewStrikesCommand implements CommandExecutor {
    private Logger log;
    private Storage store;
    private Server server;
    private int maxStrikes;
    private int timeout;

    public ViewStrikesCommand(Logger log, Storage store, Server server, int maxStrikes, int timeout) {
        this.log = log;
        this.store = store;
        this.server = server;
        this.maxStrikes = maxStrikes;
        this.timeout = timeout;
    }

    protected ArgumentParser createArgParser() {
        ArgumentParser args = ArgumentParsers.newFor("blacklist.strike")
                .build()
                .defaultHelp(false);

        args.addArgument("user")
                .nargs("?")
                .setDefault("<default>")
                .help("User name.");

        return args;
    }

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {
        Namespace ns;
        try {
            ns = createArgParser().parseArgs(Utils.arrayParse(args));
        } catch (ArgumentParserException e) {
            String message = e.getMessage();
            if (message.equals("Help Screen")) {
                message = createArgParser().formatHelp();
            }
            sender.sendMessage(String.format("\u00A7c\u00A7lERROR:\u00A7r %s", message));
            return false;
        }

        String senderName = ns.getString("user");
        if (sender instanceof Player p) {
            if (Objects.equals(senderName, "<default>")) {
                senderName = p.getName();
            }

            if (!Objects.equals(senderName, p.getName()) && !LuckPermsAccessor.hasAccessTo(p.getUniqueId(), "blacklist.strikes")) {
                sender.sendMessage("\u00A7c\u00A7lERROR:\u00A7r You doesn't have access to view strikes of other players");
                return false;
            }
        } else if (Objects.equals(senderName, "<defualt>")) {
            sender.sendMessage("\u00A7c\u00A7lERROR:\u00A7r You must set an user");
            return false;
        }

        try {
            int count = store.countStrikes(senderName, timeout);
            int countTotal = store.countStrikes(senderName, timeout);
            sender.sendMessage(String.format("\u00A7n\u00A7lTotal Strikes:\u00A7r %d", countTotal));
            sender.sendMessage(String.format("\u00A7n\u00A7lStrikes:\u00A7r %d/%d", count, maxStrikes));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return true;
    }
}
