package co.everestcode.BlackList.Listener;

import co.everestcode.BlackList.Storage.Storage;
import net.kyori.adventure.text.Component;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;

import java.util.logging.Logger;

public class LoginListener implements Listener {
    private Logger log;
    private Storage store;

    public LoginListener(Logger log, Storage store) {
        this.log = log;
        this.store = store;
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerLogin(PlayerLoginEvent evt) throws Exception {
        if (evt.getResult() != PlayerLoginEvent.Result.ALLOWED) {
            return;
        }

        String user = evt.getPlayer().getName();
        String ip1 = evt.getAddress().getHostAddress();
        String ip2 = evt.getAddress().toString();

        Integer ban = store.checkBan("user", user);
        if (ban != null) {
            evt.disallow(PlayerLoginEvent.Result.KICK_BANNED, Component.text(store.getBanReason(ban)));
            log.warning(String.format("User %s trigger action ban [USER], trying to login with IP: %s", user, ip1));
            return;
        }

        ban = store.checkBan("ip", ip1);
        if (ban != null) {
            evt.disallow(PlayerLoginEvent.Result.KICK_BANNED, Component.text(store.getBanReason(ban)));
            log.warning(String.format("User %s trigger action ban [IP], trying to login with IP: %s", user, ip1));
            return;
        }

        ban = store.checkBan("ip", ip2);
        if (ban != null) {
            evt.disallow(PlayerLoginEvent.Result.KICK_BANNED, Component.text(store.getBanReason(ban)));
            log.warning(String.format("User %s trigger action ban [IP], trying to login with IP: %s", user, ip1));
            return;
        }
    }
}
