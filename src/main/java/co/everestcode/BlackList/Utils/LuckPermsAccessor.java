package co.everestcode.BlackList.Utils;

import net.luckperms.api.LuckPerms;
import net.luckperms.api.model.group.Group;
import net.luckperms.api.model.user.User;
import net.luckperms.api.node.Node;
import net.luckperms.api.util.Tristate;
import org.bukkit.Bukkit;
import org.bukkit.plugin.RegisteredServiceProvider;

import java.util.OptionalInt;
import java.util.UUID;

public class LuckPermsAccessor {
    public static LuckPerms getAPI() {
        RegisteredServiceProvider<LuckPerms> provider = Bukkit.getServicesManager().getRegistration(LuckPerms.class);
        if (provider != null) {
            return provider.getProvider();
        }
        throw new RuntimeException("Cant get LuckPerm api");
    }

    public static int getUserWeight(UUID uid) {
        User user = getAPI().getUserManager().getUser(uid);
        assert user != null;
        Group group = getAPI().getGroupManager().getGroup(user.getPrimaryGroup());

        assert group != null;
        OptionalInt weight = group.getWeight();
        if (weight.isEmpty()) {
            return 0;
        }

        return weight.getAsInt();
    }

    public static boolean hasAccessTo(UUID uid, String perm) {
        User user = getAPI().getUserManager().getUser(uid);
        assert user != null;
        Group group = getAPI().getGroupManager().getGroup(user.getPrimaryGroup());

        assert group != null;
        Tristate contains = group.data().contains(
                Node.builder(perm).value(true).build(),
                (o1, o2) -> o1.getValue() == o2.getValue()
        );
        return contains.asBoolean();
    }
}
