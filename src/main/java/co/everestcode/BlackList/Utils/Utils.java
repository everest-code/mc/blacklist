package co.everestcode.BlackList.Utils;

import net.kyori.adventure.text.Component;
import net.sourceforge.argparse4j.inf.Namespace;
import org.bukkit.Server;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

public class Utils {
    public static String[] arrayParse(String[] args) {
        ArrayList<String> coll = new ArrayList<String>();
        StringBuilder cache = null;
        String startChar = "";
        for (int i = 0; i < args.length; i++) {
            String arg = args[i];
            if (cache != null) {
                if (arg.endsWith(startChar)) {
                    cache.append(arg, 0, arg.length() - 1);
                    coll.add(cache.toString().trim());
                    cache = null;
                    startChar = "";
                } else {
                    cache.append(arg + " ");
                }
            } else if (arg.startsWith("'") || arg.startsWith("\"")) {
                startChar = arg.substring(0, 1);
                cache = new StringBuilder(arg.substring(1) + " ");
            } else {
                coll.add(arg);
            }
        }

        return coll.toArray(new String[0]);
    }

    public static void kickUsers(Namespace ns, Server server) {
        String value = ns.getString("value");
        String reason = ns.getString("reason");
        if (Objects.equals(ns.getString("type"), "ip")) {
            server.getOnlinePlayers().forEach(player -> {
                boolean isBanned = Objects.requireNonNull(player.getAddress()).getAddress().getHostAddress().equals(value);
                isBanned = isBanned || Objects.requireNonNull(player.getAddress()).getAddress().toString().equals(value);
                if (isBanned) player.kick(Component.text(reason));
            });
        } else {
            Player player = server.getPlayer(value);
            if (player != null && player.isOnline()) {
                player.kick(Component.text(reason));
            }
        }
    }
}
