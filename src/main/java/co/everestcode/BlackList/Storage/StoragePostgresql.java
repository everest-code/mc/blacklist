package co.everestcode.BlackList.Storage;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.bukkit.configuration.ConfigurationSection;

import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.Date;
import java.util.TimeZone;
import java.util.logging.Logger;

public class StoragePostgresql implements Storage {
    protected Logger logger;

    public StoragePostgresql(Logger log) {
        logger = log;
    }

    protected String prefix;
    protected Connection conn;
    private ConfigurationSection messages;

    @Override
    public void initialize(ConfigurationSection conf) throws SQLException, IOException {
        this.prefix = conf.getString("prefix");

        HikariConfig dbconf = new HikariConfig();
        dbconf.setDataSourceClassName("org.postgresql.ds.PGSimpleDataSource");
        dbconf.addDataSourceProperty("serverName", conf.getString("cred.host"));
        dbconf.addDataSourceProperty("portNumber", String.valueOf(conf.getInt("cred.port")));
        dbconf.addDataSourceProperty("databaseName",  conf.getString("cred.database"));
        dbconf.addDataSourceProperty("user",  conf.getString("cred.user"));
        dbconf.addDataSourceProperty("password",  conf.getString("cred.password"));

        HikariDataSource dbSource = new HikariDataSource(dbconf);
        conn = dbSource.getConnection();

        String sql = "SELECT COUNT(*) FROM pg_tables WHERE tablename = ?";
        PreparedStatement pstmt = conn.prepareStatement(sql);
        pstmt.setString(1, String.format("%sbans", prefix));

        ResultSet res = pstmt.executeQuery();
        res.next();
        boolean exists = res.getInt(1) > 0;
        logger.info(String.format("Database schema exists: %s", exists));
        if (!exists) {
            logger.warning("Database schema not created, migrating...");
            InputStream f = getClass().getClassLoader().getResourceAsStream("schemas/postgresql.sql");
            int c;
            sql = "";
            while ((c = f.read()) != -1) {
                sql += String.valueOf((char) c);
            }
            f.close();

            String[] sqls = sql.replaceAll("\\[prefix\\]", getPrefix()).split(";");
            for (int i = 0; i < sqls.length; i++) {
                String isql = sqls[i];
                Statement stmt = conn.createStatement();
                stmt.execute(isql);
                stmt.close();
            }
        }
        res.close();
        pstmt.close();
    }

    @Override
    public void setDefaultMessages(ConfigurationSection conf) {
        messages = conf;
    }

    @Override
    public Connection getConn() {
        return this.conn;
    }

    @Override
    public String getPrefix() {
        if (this.prefix == null) {
            return "";
        }
        return this.prefix;
    }

    @Override
    public String getMessage(String type) {
        return messages.getString(type);
    }

    @Override
    public int createBan(String type, String value, String punisher, String reason, Date expireDate) throws Exception {
        String sql = String.format("INSERT INTO %sbans (type, value, punisher, reason, expire_at) VALUES (?, ?, ?, ?, ?::TIMESTAMPTZ) RETURNING id", getPrefix());
        if (expireDate == null) {
            sql = String.format("INSERT INTO %sbans (type, value, punisher, reason, expire_at) VALUES (?, ?, ?, ?, NULL) RETURNING id", getPrefix());
        }

        PreparedStatement stmt = getConn().prepareStatement(sql);
        stmt.setString(1, type);
        stmt.setString(2, value);
        stmt.setString(3, punisher);
        stmt.setString(4, reason);
        if (expireDate != null) {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
            formatter.setTimeZone(TimeZone.getTimeZone(ZoneId.systemDefault()));
            stmt.setString(5, formatter.format(expireDate));
        }

        ResultSet res = stmt.executeQuery();
        if (!res.next()) {
            res.close();
            stmt.close();
            throw new RuntimeException("No ban was added, check database connection");
        }
        int id = res.getInt(1);
        res.close();
        stmt.close();

        return id;
    }

    @Override
    public int createBan(String type, String value, String punisher, Date expireDate) throws Exception {
        return createBan(type, value, punisher, getMessage("ban"), expireDate);
    }

    @Override
    public int createBan(String type, String value, String punisher, String reason) throws Exception {
        return createBan(type, value, punisher, reason, null);
    }

    @Override
    public int createBan(String type, String value, String punisher) throws Exception {
        return createBan(type, value, punisher, getMessage("blacklist"), null);
    }

    @Override
    public Integer checkBan(String type, String value) throws SQLException {
        String sql = String.format("SELECT id FROM %sbans WHERE type = ? AND value = ?", getPrefix());
        PreparedStatement stmt = getConn().prepareStatement(sql);

        stmt.setString(1, type);
        stmt.setString(2, value);

        ResultSet res = stmt.executeQuery();
        Integer id = null;
        if (res.next()) {
            id = res.getInt(1);
        }
        res.close();
        stmt.close();

        return id;
    }

    @Override
    public String getBanReason(int id) throws SQLException {
        String sql = String.format("SELECT reason FROM %sbans WHERE id = ?", getPrefix());
        PreparedStatement stmt = conn.prepareStatement(sql);

        stmt.setInt(1, id);

        ResultSet res = stmt.executeQuery();
        String reason = null;
        if (res.next()) {
            reason = res.getString(1);
        }
        res.close();
        stmt.close();

        return reason;
    }

    @Override
    public void removeExpiredBans() throws SQLException {
        String sql = String.format("DELETE FROM %sbans WHERE expire_at != NULL AND expire_at < NOW()", getPrefix());
        Statement stmt = getConn().createStatement();
        stmt.execute(sql);
        stmt.close();
    }

    @Override
    public void deleteBan(int id) throws SQLException {
        String sql = String.format("DELETE FROM %sbans WHERE id = ?", getPrefix());
        PreparedStatement stmt = getConn().prepareStatement(sql);
        stmt.setInt(1, id);

        stmt.execute();
        stmt.close();
    }

    @Override
    public void deleteBan(String value) throws SQLException {
        String sql = String.format("DELETE FROM %sbans WHERE value = ?", getPrefix());
        PreparedStatement stmt = getConn().prepareStatement(sql);
        stmt.setString(1, value);

        stmt.execute();
        stmt.close();
    }

    @Override
    public int createStrike(String punisher, String user) throws SQLException {
        String sql = String.format("INSERT INTO %sstrikes (punisher, user) VALUES (?, ?) RETURNING id", getPrefix());
        PreparedStatement stmt = getConn().prepareStatement(sql);
        stmt.setString(1, punisher);
        stmt.setString(2, user);

        ResultSet res = stmt.executeQuery();
        if (!res.next()) {
            res.close();
            stmt.close();
            throw new RuntimeException("No strike was added, check database connection");
        }
        int id = res.getInt(1);
        res.close();
        stmt.close();

        return id;
    }

    @Override
    public int countStrikes(String user, int timeout) throws SQLException {
        String sql = String.format("SELECT COUNT(id) FROM %sstrikes WHERE user = ? AND created_at > NOW() - ?::INTERVAL", getPrefix());
        PreparedStatement stmt = getConn().prepareStatement(sql);
        stmt.setString(1, user);
        stmt.setString(2, String.format("%d DAYS", timeout));

        ResultSet res = stmt.executeQuery();
        int count = res.getInt(1);
        res.close();
        stmt.close();

        return count;
    }

    public int countStrikes(String user) throws SQLException {
        String sql = String.format("SELECT COUNT(id) FROM %sstrikes WHERE user = ?", getPrefix());
        PreparedStatement stmt = getConn().prepareStatement(sql);
        stmt.setString(1, user);

        ResultSet res = stmt.executeQuery();
        int count = res.getInt(1);
        res.close();
        stmt.close();

        return count;
    }
}
