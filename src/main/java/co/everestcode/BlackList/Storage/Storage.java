package co.everestcode.BlackList.Storage;


import org.bukkit.configuration.ConfigurationSection;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;

public interface Storage {
    public void initialize(ConfigurationSection conf) throws Exception;
    public void setDefaultMessages(ConfigurationSection conf);
    public Connection getConn();
    public String getPrefix();
    // Only available for: "ban", "blacklist", "strike", "ban_strike"
    public String getMessage(String type);

    // Bans & blacklist
    public int createBan(String type, String value, String punisher, String reason, Date expireDate) throws Exception;
    public int createBan(String type, String value, String punisher, Date expireDate) throws Exception;
    public int createBan(String type, String value, String punisher, String reason) throws Exception;
    public int createBan(String type, String value, String punisher) throws Exception;
    public Integer checkBan(String type, String value) throws Exception;
    public String getBanReason(int id) throws Exception;
    public void removeExpiredBans() throws Exception;
    public void deleteBan(int id) throws Exception;
    public void deleteBan(String value) throws Exception;

    public int createStrike(String punisher, String user) throws Exception;
    public int countStrikes(String user, int timeout) throws Exception;
    public int countStrikes(String user) throws Exception;
}
