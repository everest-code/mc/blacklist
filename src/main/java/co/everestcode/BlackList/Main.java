package co.everestcode.BlackList;

import co.everestcode.BlackList.Command.*;
import co.everestcode.BlackList.Listener.LoginListener;
import co.everestcode.BlackList.Storage.*;
import co.everestcode.BlackList.Timer.DeleteBansTimer;
import org.bukkit.command.CommandExecutor;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Objects;

public final class Main extends JavaPlugin {
    Storage store;

    @Override
    public void onEnable() {
        File file = new File(getDataFolder(), "config.yml");
        if (!file.exists()) {
            saveDefaultConfig();
        } else {
            try {
                getConfig().load(file);
            } catch (IOException | InvalidConfigurationException e) {
                throw new RuntimeException(e);
            }
        }

        String engine = getConfig().getString("database.engine");
        assert engine != null;

        getLogger().info(String.format("Initializing DB with engine [%s]", engine.toUpperCase()));
        switch (engine.toLowerCase()) {
            case "postgresql":
                store = new StoragePostgresql(getLogger());
                break;
            case "sqlite":
                store = new StorageSqlite(getLogger(), getDataFolder());
                break;
            default:
                String err = String.format("Engine [%s] has no handler", engine.toUpperCase());
                throw new RuntimeException(err);
        }

        store.setDefaultMessages(getConfig().getConfigurationSection("messages"));

        try {
            store.initialize(getConfig().getConfigurationSection("database"));
            getLogger().info("Database initialized");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        enableCommands();
        enableListeners();
        enableTimers();
    }

    public void enableCommands() {
        CommandExecutor banCmd = new BanCommand(getLogger(), store, getServer(), getConfig().getInt("timeout.ban"));
        CommandExecutor unbanCmd = new UnbanCommand(getLogger(), store);
        CommandExecutor unblackCmd = new UnbanCommand(getLogger(), store, "Blacklist entry");
        CommandExecutor blackCmd = new BlacklistCommand(getLogger(), store, getServer());
        CommandExecutor strikeCmd = new StrikeCommand(getLogger(), store, getServer(), getConfig().getInt("max_strikes"), getConfig().getInt("timeout.strike"), getConfig().getInt("timeout.ban_strike"));
        CommandExecutor strikesCmd = new ViewStrikesCommand(getLogger(), store, getServer(), getConfig().getInt("max_strikes"), getConfig().getInt("timeout.strike"));
        Objects.requireNonNull(getCommand("ban")).setExecutor(banCmd);
        Objects.requireNonNull(getCommand("unban")).setExecutor(unbanCmd);
        Objects.requireNonNull(getCommand("unblack")).setExecutor(unblackCmd);
        Objects.requireNonNull(getCommand("black")).setExecutor(blackCmd);
        Objects.requireNonNull(getCommand("strike")).setExecutor(strikeCmd);
        Objects.requireNonNull(getCommand("strikes")).setExecutor(strikesCmd);
    }

    public void enableListeners() {
        Listener l = new LoginListener(getLogger(), store);
        getServer().getPluginManager().registerEvents(l, this);
    }

    public void enableTimers() {
        Runnable r = new DeleteBansTimer(store);
        getServer().getScheduler().runTaskTimer(this, r, 600L, 6000L);
    }

    @Override
    public void onDisable() {
        if (store != null) {
            try {
                if (store.getConn() != null) {
                    getLogger().info("Closing database");
                    store.getConn().close();
                }
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
